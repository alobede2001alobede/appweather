


from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from styeling import Style
from help import Help
import time
from datetime import datetime
import requests

class Main:
    """ The main file is responsible for data, input and output """
    def __init__(self,root):
        self.root = root
        self.object_help = Help(self.root)
        self.url = 'https://api.openweathermap.org/data/2.5/weather?q={}&appid={}'
        self.API_KEY = '7224f2f0ba8bfcd1ac7ea34912ccdb65'

        self.variable_city_name = StringVar()



        self.root.geometry('1000x650+450+100')
        self.root.resizable(width=False,height=False)
        self.root.title('Weather Forecast')
        #self.root.iconbitmap('images\\icon_weather.ico')
        self.object_styleing = Style(self.root)
        #---------------------------------------------------


        self.entry_city = Entry(
            self.root , justify='center' , width=17 ,
            font=('poppins',23,'bold') , bg='#404040',border=0 , fg='white',textvariable=self.variable_city_name
            )
        self.entry_city.place(x=330,y=40)



        # The Button Is Get City Name
        self.icon_get_data = PhotoImage(file='images\\search_now.png')
        self.button_data = Button(self.root ,image=self.icon_get_data,borderwidth=0,cursor='hand2',bg='#404040',command=self.get_data)
        self.button_data.place(x=630,y=34)



    def get_data(self):
        try :

            self.url_api = requests.get(self.url.format(self.entry_city.get() , self.API_KEY))
            if self.url_api :
                self.jsonData = self.url_api.json()
                self.city = self.jsonData['name']
                self.country = self.jsonData['sys']['country']

                self.temp_klv = self.jsonData['main']['temp']
                self.temp_celcius = (self.temp_klv - 273.15)
                self.temp_fehrenheit = (self.temp_klv - 273.15)*9/5+32

                self.weather = self.jsonData['weather'][0]['main']
                self.pressure = self.jsonData['main']['pressure']
                self.description = self.jsonData['weather'][0]['description']
                self.humidity = self.jsonData['main']['humidity']


                self.final_result = (
                    self.city , self.country ,
                    self.temp_klv , self.temp_celcius ,
                    self.temp_fehrenheit , self.weather ,
                    self.pressure , self.description,
                    self.humidity


                    )

                if self.final_result :
                    self.search()

                    #°F
            else:
                return messagebox.showwarning(title='Invalid', message='Error Please check the correct city name')
        except requests.exceptions.ConnectionError:
            pass

    def search(self):
        if self.final_result :
           self.object_styleing.lable_1['text'] = self.final_result[0]
           self.object_styleing.lable_2['text'] = ('{:.0f}°C , {:.0f}°F'.format(self.final_result[3],self.final_result[4]))
           self.object_styleing.lable_3['text'] = self.final_result[5]
           self.object_styleing.lable_4['text'] = self.final_result[6]
           self.object_styleing.lable_5['text'] = self.final_result[7]
           self.object_styleing.lable_6['text'] = self.final_result[8]
           self.object_styleing.lable_humidity['text'] = 'Humidity |'

           self.weather_lable = Label(self.root , font=('bold',30),bg='white',fg='red',text='{:.0f}°C'.format(self.final_result[3]))
           self.weather_lable.place(x=500,y=200)
           self.add_time()


           self.currentw_eather = Label(self.root , font=('bold',18),bg='white',fg='black',text='Current Weather')
           self.currentw_eather.place(x=150,y=200)


    def add_time(self):


        self.clock_time = datetime.now()
        self.string_time = self.clock_time.strftime('%H:%M:%S %p')


        self.label_clock_time = Label(self.root ,
                                        background='white'  ,
                                        font=('Impact, fantasy',20),
                                        text=f'{self.string_time}').place(x=150,y=280)
        self.root.after(1000, self.add_time)


if __name__ == '__main__':
    app = Tk()
    obj = Main(app)
    app.mainloop()
