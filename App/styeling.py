


from tkinter import *
from PIL import Image , ImageTk
import time

class Style:
    """ File for selecting the shape of the application and adding images """
    def __init__(self,master):
        self.master = master
        self.variable_city_name = StringVar()

        self.master.configure(background='white')

        self.icon_search = PhotoImage(file='images\\search.png')
        self.label_search = Label(self.master , image=self.icon_search,bg='white')
        self.label_search.place(x=250,y=20)

        #Add Image Logo

        self.logo_image = PhotoImage(file='images\\logo.png')
        self.label_logo = Label(self.master , image=self.logo_image,background='white')
        self.label_logo.place(x=300,y=200)


        #Add Image Box Prrutcher

        self.ico_box = PhotoImage(file='images\\box.png')
        self.label_box = Label(self.master , image=self.ico_box,background='white')
        self.label_box.place(x=100,y=500)

        # Label Box

        self.location_label = Label(self.master , text='Location' , font=('poppins',12,'bold'),background='#1ab5ef',fg='white')
        self.location_label.place(x=150,y=520)

        self.temperature_label = Label(self.master , text='Temperature' , font=('poppins',12,'bold'),background='#1ab5ef',fg='white')
        self.temperature_label.place(x=280,y=520)



        self.weather_label = Label(self.master , text='Weather' , font=('poppins',12,'bold'),background='#1ab5ef',fg='white')
        self.weather_label.place(x=450,y=520)

        self.pressure_label = Label(self.master , text='Pressure' , font=('poppins',12,'bold'),background='#1ab5ef',fg='white')
        self.pressure_label.place(x=590,y=520)



        self.description_label = Label(self.master , text='Description' , font=('poppins',12,'bold'),background='#1ab5ef',fg='white')
        self.description_label.place(x=740,y=520)


        # ------------- Create Result None In The First --------------------------------

        self.lable_1 = Label(self.master , text='....',font=('poppins',12,'bold'),background='#1ab5ef',fg='#000000')
        self.lable_1.place(x=150,y=550)

        self.lable_2 = Label(self.master , text='....',font=('poppins',12,'bold'),background='#1ab5ef',fg='#000000')
        self.lable_2.place(x=290,y=550)


        self.lable_3 = Label(self.master , text='....',font=('poppins',12,'bold'),background='#1ab5ef',fg='#000000')
        self.lable_3.place(x=460,y=550)

        self.lable_4 = Label(self.master , text='....',font=('poppins',12,'bold'),background='#1ab5ef',fg='#000000')
        self.lable_4.place(x=610,y=550)


        self.lable_5 = Label(self.master , text='....',font=('poppins',12,'bold'),background='#1ab5ef',fg='#000000')
        self.lable_5.place(x=730,y=550)

        self.lable_6 = Label(self.master ,font=('poppins',20,'bold'),background='white',fg='#000000')
        self.lable_6.place(x=630,y=280)


        self.lable_humidity = Label(self.master ,font=('poppins',15,'bold'),bg='white',fg='#8c8df1')
        self.lable_humidity.place(x=516,y=280)
