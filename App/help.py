

from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import webbrowser

class Help:
	""" Contact file for the application developer """
	def __init__(self,par):
		self.par = par

		self.menubar = Menu(self.par)
		self.file = Menu(self.menubar,tearoff=False)
		self.file.add_command(label='Save As')

		self.menubar.add_cascade(label='Help',command=self.connect)
		self.par.config(menu=self.menubar)

	def connect(self):
		self.webbrowser = webbrowser.open_new('https://www.facebook.com/alaa.jassim.mohammed/')
